package iitb.ac.in;

import iitb.ac.in.Utilities.MapUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
//arpuadrrnaa
public class Main 
{
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) 
	{
		if(args.length != 2)
		{
			System.out.println("Usage: vocabulary vectors");
			System.exit(-1);
		}

		String vocabulary = args[0];
		String vectorFile = args[1];
		Map<String, Integer> Dictionary = new TreeMap<>();
		
		ArrayList<WordVector> examples = new ArrayList<WordVector>();
		
		try
		{
			BufferedReader vocabReader = new BufferedReader(new FileReader(vocabulary));
			BufferedReader vectorReader = new BufferedReader(new FileReader(vectorFile));
					
			String vocab = null;
			String vector = null;
			
			WordVector temp;// = new WordVector();
			
			int  index = 0;
			while((vocab = vocabReader.readLine()) != null && (vector = vectorReader.readLine()) != null)
			{
				temp = new WordVector();
				
				temp.vector.clear();
				
				temp.word = vocab.trim();
				
				vector = vector.trim();
				
				if(temp.word.length() == 0)
					continue;
				
				Dictionary.put(temp.word, index);
				
				String[] vecValues = vector.split(" ");
				
				double sum = 0.0;
				for(int i = 0; i < vecValues.length; i++)
				{
					sum += Double.parseDouble( vecValues[i]) * Double.parseDouble( vecValues[i]);
				}
				
				for(int i = 0; i < vecValues.length; i++)
				{
					if(sum != 0.0)
						temp.vector.add(Double.parseDouble( vecValues[i]) / Math.sqrt(sum));
					else temp.vector.add(0.0);
				}
				
				examples.add(temp);
				index++;
			}
			vocabReader.close();
			vectorReader.close();
			System.out.println("read " + examples.size());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
//		for(int i = 0; i < examples.size(); i++)
//		{
//			System.out.println(examples.get(i).word);
//		}
		
		while(true)
		{
			System.out.println("Enter a word");
			
			BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
			
			try
			{
				String word = console.readLine().trim();
				
				if(!Dictionary.containsKey(word))
				{
					System.out.println("Word not in vocabulary");
					continue;
				}
				
				int index = Dictionary.get(word);
				
				double temp = 0.0;
				Map<String, Double> scores = new TreeMap<String, Double>();
								
				for(int i = 0; i < examples.size(); i++)
				{
					temp = DistanceMeasure.cosineSimilarity(examples.get(i), examples.get(index));
					//temp = DistanceMeasure.euclideanDistance(examples.get(i), examples.get(index));
					
					scores.put(examples.get(i).word, temp);
				}
				
				scores = MapUtil.sortByValue(scores);
				
				Iterator it  = null;
				it =  scores.entrySet().iterator();
	    		
				int count  = 0;
	    		while(it.hasNext())
	    		{
	    			count ++;
	    			
	    			if(count > 10)
	    				break;
	    			
	    			Map.Entry pairs = (Map.Entry) it.next();
	    			
	    			System.out.println(pairs.getKey() + "\t"  + pairs.getValue() );
	    		}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
