package iitb.ac.in;

public class DistanceMeasure 
{
	static double cosineSimilarity(WordVector w1, WordVector w2)
	{
		double distance = 0.0;
		
		if(w1 == null || w2 == null)
		{
			System.out.println("Either w1 or w2 is null");
			return -1;
		}
		
		if(w1.vector.size() != w2.vector.size())
		{
			System.out.println(w1.word + "\t" + w2.word + " Dimensions not same");
			return -1;
		}
		
		for(int i = 0; i < w1.vector.size(); i++)
		{
			distance += w1.vector.get(i) * w2.vector.get(i);
		}
		return distance;
	}
	
	static double euclideanDistance(WordVector w1, WordVector w2)
	{
		double distance = 0.0;
		
		if(w1 == null || w2 == null)
		{
			System.out.println("Either w1 or w2 is null");
			return -1;
		}
		
		if(w1.vector.size() != w2.vector.size())
		{
			System.out.println(w1.word + "\t" + w2.word + " Dimensions not same");
			return -1;
		}
		
		for(int i = 0; i < w1.vector.size(); i++)
		{
			distance += (w1.vector.get(i) - w2.vector.get(i)) * (w1.vector.get(i) - w2.vector.get(i));
		}
		
		return Math.sqrt(distance);
	}

}
